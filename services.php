<?php include('includes/header.php'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Type of services
        
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="servicesTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Type of service</th>
                </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('includes/footer.php'); ?>
  <script type="text/javascript">
    $(function () {


      $('#servicesTbl').DataTable().destroy()
      $('#servicesTbl').dataTable({
          "processing": true,
          "ajax": "fetch_services.php",
          "columns": [
            { data: 'service',
                      render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  }},
              {data: 'service'}
          ]
      });


    })


  </script>

  
