<?php include('includes/header.php'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Patients
        
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-9">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="patientTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Date Of Birth</th>
                  <th>Gender</th>
                  <th>Type of Service</th>
                  <th>General Comments</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-3">
          <form id="patient_frm">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">New Patient details</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                
                  <!-- text input -->
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter name of the patient...">
                  </div>
                  <div class="form-group">
                    <label>Date of birth</label>                   
                      <input type="date" name="date_of_birth" id="dob"  class="form-control pull-right">
                  </div>
                  
                  <div class="form-group">
                    <label>Gender</label>
                    <select class="form-control" name="gender" id="gender">
                      <option value="" disabled="" selected="">Select Gender</option>
                    </select>
                  </div>

                  <!-- select -->
                  <div class="form-group">
                    <label>Type of service</label>
                    <select class="form-control" name="type_of_service" id="services">
                      <option value="" disabled="" selected="">Select type of service</option>
                      <option value="1">Outpatient</option>
                      <option value ="2">In patient</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>General Comments</label>
                    <textarea class="form-control" id="comments" rows="3" name="general_comments" placeholder="General comments for this patient..."></textarea>
                  </div>


                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <!-- <div class="form-group"> -->
                  <button type="submit" class="btn btn-block btn-success">Submit</button>
                <!-- </div> -->
              </div>
            </div>
          </form>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php include('includes/footer.php'); ?>
  <script type="text/javascript">
    $(function () {


      loadPatients()
      populateGenders()
      populateSevices()

      $('#patient_frm').on('submit', function(event){
        event.preventDefault();

        if(!$('#name').val() || !$('#dob').val() || !$('#gender').val() || !$('#services').val() || !$('#comments').val())
        {
          alert('All fields are required!')
        }else{
          $.ajax({
            type:'POST',
            url:'save_patient.php',
            cache: false,
            data:$(this).serialize(),

            success:function(data) {

              $('#name').val('')
              $('#dob').val('')
              $('#gender').val('').change()
              $('#service').val('').change()
              $('#comments').val('')

              loadPatients()
              alert(data)
            },
            error: function(xhr, status, error){
              console.error(xhr);
            }
          });
        }

        

      });


    })


    function loadPatients()
    {

      // $('#patientTbl').DataTable().destroy()
      $('#patientTbl').dataTable({
          "destroy": true,
            "processing": true,
            "serverSide": true,
            "order":[],
            "aoColumnDefs":[
                {
                    "aTargets":[],
                    "mData": null
                }
            ],

          "ajax": "fetch_patients.php",
          "columns": [
              {data: 'name'},
              {data: 'dob'},
              {data: 'gender'},
              {data: 'service'},
              {data: 'general_comments'}
          ]
      });

    }

    function populateGenders()
    {
      var genders = "";
      $.get('fetch_genders.php', function (response) {

          var genderData = JSON.parse(response);

          genders+="<option value=''>"+'Select Gender'+"</option>";

          $.each(genderData.data, function(k, v) {
   
              genders+="<option value='"+v.id+"'>"+v.gender+"</option>";      
          }); 

          $("#gender").html(genders);
      });
    }

    function populateSevices()
    {
      var services = "";
      $.get('fetch_services.php', function (response) {
        
          var servicesData = JSON.parse(response);

          services+="<option value=''>"+'Select Service'+"</option>";

          $.each(servicesData.data, function(k, v) {
   
              services+="<option value='"+v.id+"'>"+v.service+"</option>";      
          }); 

          $("#services").html(services);
      });
    }

  </script>

  
