1. Unzip the folder to your server
2. create an empty database and import contents of bliss_dev_db.sql(the database has tbl_service and tbl_gender) populated.
3. Modify db_connection.php file inside the includes folder to add correct username and password for your database connection.
4. Access the application from yourserverUrl/bliss-dev-app

You can also clone the application using the link below
git clone https://salaash@bitbucket.org/salaash/bliss-dev-app.git