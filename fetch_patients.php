<?php

include('includes/db_connection.php');
$conn = OpenCon();


$sql = "select name, date_of_birth as dob, tbl_gender.gender as gender, tbl_service.type_of_service as service, general_comments from tbl_patient LEFT JOIN tbl_gender on tbl_patient.gender_id = tbl_gender.id
	LEFT JOIN tbl_service on tbl_patient.service_id = tbl_service.id
	";



$result = mysqli_query($conn, $sql);

$array = array();

while($row = mysqli_fetch_assoc($result)) {
    $array[] = $row;
}

$dataset = array(
    "echo" => 1,
    "totalrecords" => count($array),
    "totaldisplayrecords" => count($array),
    "data" => $array
);

echo json_encode($dataset);


CloseCon($conn);